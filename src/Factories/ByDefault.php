<?php
namespace Ciebit\Pagination\Factories;

use Ciebit\Pagination\Views\View;
use Ciebit\Pagination\Views\ByDefault as DefaultView;
use Ciebit\Pagination\Paginator;
use Ciebit\DomTemplate;
use TypeError;

class ByDefault
{
    private $view; #View
    private $html_path = __DIR__."/../../exemplos/layout.html"; #string

    public function build(int $totalRecords, int $recordsPerPage, int $currentPage): View
    {
        $Paginator = new Paginator;
        $Paginator->setTotalRecords($totalRecords);
        $Paginator->setRecordsPerPage($recordsPerPage);
        $Paginator->setCurrentPage($currentPage);

        $DomTemplate = new DomTemplate();
        $DomTemplate->setHtmlByFile($this->html_path);

        $View = new $this->view($Paginator, $DomTemplate);

        return $View;
    }

    public function setView(string $view): self
    {
        if (! is_subclass_of($view, View::class)) {
            throw new TypeError("É esperado uma instância que estenda Ciebit\Pagination\Views\View", 1);
        }
        $this->view = $view;
        return $this;
    }

    public function setHtmlTemplate(string $path): self
    {
        $this->html_path = $path;
        return $this;
    }
}
