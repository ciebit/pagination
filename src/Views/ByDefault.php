<?php
namespace Ciebit\Pagination\Views;

class ByDefault extends View
{
    private function addItem(string $text, string $link, bool $visibility, array $classes): self
    {
        $Item = $this->getModel();

        $this->setClassVisibility($Item, $visibility);
        $Item->setAttribute('href', $this->url_prefix . $link . $this->url_sufix);
        $Item->textContent = $text;

        $Item->className = "paginador-botao";
        for ($i=0; $i < count($classes); $i++) {
            $Item->classList->add($classes[$i]);
        }

        $Container = $this->getContainer();
        $Container->appendChild($Item);

        return $this;
    }

    public function update(): View
    {
        $this->clearItems();

        $this->updateButtonFirst();
        $this->updateButtonPrevious();
        $this->updateButtonsItems();
        $this->updateButtonNext();
        $this->updateButtonLast();

        return $this;
    }

    private function updateButtonFirst(): self
    {
        $this->addItem('primeira', "1", $this->visibility_button_first, array($this->selector_button_first));

        return $this;
    }

    private function updateButtonLast(): self
    {
        $last_page = $this->getTotalButtons();
        $this->addItem('última', $last_page, $this->visibility_button_last, array($this->selector_button_last));

        return $this;
    }

    private function updateButtonNext(): self
    {
        $current_page = intval($this->paginator->getCurrentPage());
        if ($current_page == $this->getTotalButtons()) {
            $next_page = "";
        } else {
            $next_page = $current_page + 1;
        }
        $this->addItem('próxima', $next_page, $this->visibility_button_next, array($this->selector_button_next));

        return $this;
    }

    private function updateButtonPrevious(): self
    {
        $current_page = intval($this->paginator->getCurrentPage());
        if ($current_page == 1) {
            $previous_page = "";
        } else {
            $previous_page = $current_page - 1;
        }
        $this->addItem('anterior', $previous_page, $this->visibility_button_previous, array($this->selector_button_previous));

        return $this;
    }

    private function updateButtonsItems(): self
    {
        $buttons_number = $this->getTotalButtons();

        $x = 1;
        for ($i=0; $i < $buttons_number; $i++) {
            $this->addItem($x, $x, $this->visibility_button_items, array($this->selector_buttons_items));
            $x++;
        }

        return $this;
    }

}
