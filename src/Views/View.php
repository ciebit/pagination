<?php
namespace Ciebit\Pagination\Views;

use Ciebit\Pagination\Paginator;
use Ciebit\DomTemplate;
use Gt\Dom\Element;
use Exception;

abstract class View
{
    protected $paginator; #Paginator
    protected $dom_template; #DomTemplate
    protected $selector_buttons_container = ".paginador-container";
    protected $selector_all_items = ".paginador-botao";
    protected $selector_button_first = "paginador-botao-primeiro";
    protected $selector_button_previous = ".paginador-botao-anterior";
    protected $selector_button_next = ".paginador-botao-proximo";
    protected $selector_button_last = ".paginador-botao-ultimo";
    protected $selector_buttons_items = "";
    protected $url_prefix = "./";
    protected $url_sufix = "/";
    protected $visibility_button_first = false;
    protected $visibility_button_previous = true;
    protected $visibility_button_next = true;
    protected $visibility_button_last = true;
    protected $visibility_button_items = true;
    private $status_class = "inactived";
    private $ItemModel; #Element

    public function __construct(Paginator $paginator, DomTemplate $dom_template)
    {
        $this->paginator = $paginator;
        $this->dom_template = $dom_template;

        $this->ItemModel = $this->getModel();
    }

    public function getDomTemplate(): DomTemplate
    {
        return $this->dom_template;
    }

    public function getHtml(): string
    {
        return $this->dom_template->getHtml();
    }

    public function getPaginator(): paginator
    {
        return $this->paginator;
    }

    public function setSelectorButtonsContainer(string $selector): self
    {
        $this->selector_buttons_container = $selector;
    }

    public function setSelectorButtonFirst(string $selector): self
    {
        $this->selector_button_first = $selector;
    }

    public function setSelectorButtonLast(string $selector): self
    {
        $this->selector_button_last = $selector;
    }

    public function setSelectorButtonNext(string $selector): self
    {
        $this->selector_button_next = $selector;
    }

    public function setSelectorButtonPrevious(string $selector): self
    {
        $this->selector_button_previous = $selector;
    }

    public function setSelectorButtonsItems(string $selector): self
    {
        $this->selector_buttons_items = $selector;
    }

    public function setUrlPrefix(string $prefix): self
    {
        $this->url_prefix = $prefix;
    }

    public function setUrlSufix(string $sufix): self
    {
        $this->url_sufix = $sufix;
    }

    public function setVisibilityButtonFirst(bool $visibility): self
    {
        $this->visibility_button_first = $visibility;
    }

    public function setVisibilityButtonLast(bool $visibility): self
    {
        $this->visibility_button_last = $visibility;
    }

    public function setVisibilityButtonNext(bool $visibility): self
    {
        $this->visibility_button_next = $visibility;
    }

    public function setVisibilityButtonPrevious(bool $visibility): self
    {
        $this->visibility_button_previous = $visibility;
    }

    public function setVisibilityButtonItems(bool $visibility): self
    {
        $this->visibility_button_items = $visibility;
    }

    public function update(): self
    {
        return $this;
    }

    protected function getModel(): Element
    {
        if ($this->ItemModel) {
            return $this->ItemModel->cloneNode(true);
        }

        $ItemModel = $this->getContainer()->querySelector($this->selector_all_items);

        if (! ($ItemModel instanceof Element)) {
            throw new Exception("O item do template fornecido não foi encontrado", 1);
        }

        $this->ItemModel = $ItemModel->cloneNode(true);
        return $this->ItemModel->cloneNode(true);
    }

    protected function clearItems(): self
    {
        $Container = $this->getContainer();
        $Items = $Container->querySelectorAll($this->selector_all_items);

        for ($i = $Items->count() -1; $i >= 0 ; $i--) {
            $Items->item($i)->remove();
        }

        return $this;
    }

    protected function getContainer(): Element
    {
        return $this->getDomTemplate()->getElement($this->selector_buttons_container);
    }

    protected function getTotalButtons(): int
    {
        $total_records = $this->paginator->getTotalRecords();
        $records_per_page = $this->paginator->getRecordsPerPage();

        $total_buttons = ceil($total_records / $records_per_page);

        return $total_buttons;
    }

    protected function setClassVisibility($element, $visibility): self
    {
        if (!$visibility) {
            $element->classList->add($this->status_class);
        } else {
            $element->classList->remove($this->status_class);
        }
        return $this;
    }
}
