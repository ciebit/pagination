<?php
namespace Ciebit\Pagination;

use \Psr\Http\Message\ServerRequestInterface;
use Ciebit\Paginator\Views\View;

class Paginator
{
    private $records_total; #int
    private $records_page; #int
    private $current_page; #int

    public function getCurrentPage(): int
    {
        return $this->current_page;
    }

    public function getRecordsPerPage(): int
    {
        return $this->records_page;
    }

    public function getTotalRecords(): int
    {
        return $this->records_total;
    }

    public function setCurrentPage(int $page): self
    {
        $this->current_page = $page;
        return $this;
    }

    public function setTotalRecords(int $total): self
    {
        $this->records_total = $total;
        return $this;
    }

    public function setRecordsPerPage(int $num): self
    {
        $this->records_page = $num;
        return $this;
    }
}
