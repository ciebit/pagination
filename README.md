# Paginador

Pacote de geração de uma estrutura HTML para paginar listas.


## Recursos

* Cria a estrutura HTML com base em classes, dando total liberdade de alteração do HTML


## Exemplo

```
require 'vendor/autoload.php';

use \Ciebit\Paginador;

$Paginador = new Paginador(100, 30, $Requisicao);

$Paginador->iniciar();

echo $Paginador->obterVisao()->obterHtml();

```

## Documentação

A documentação poderá ser encontra em https://bitbucket.org/ciebit/paginador/wiki/Home
