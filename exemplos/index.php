<?php
ini_set('display_errors', on);
require '../vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use \Slim\App as SlimApp;
use Ciebit\Paginador\Paginador;
use Ciebit\Paginador\Visoes\Padrao as Visao;
use \Slim\Http\Environment;
use \Slim\Http\Request;

function criaPaginacao($Request, $Response) {
    $Visao = new Visao;
    $Visao->definirLayoutArquivo('layout.html');

    $Paginador = new Paginador(100, 10, $Request);
    $Paginador->definirVisao($Visao);
    $Paginador->obterVisao()->definirModoCompacto(3);
    $Paginador->iniciar();
    $Response->getBody()->write($Paginador->obterVisao()->obterHtml());
}

$configuracoes = ['displayErrorDetails' => true];
$app = new SlimApp(['settings' => $configuracoes]);

//Rotas
$app->get('/', function(ServerRequestInterface $Request, ResponseInterface $Response) {
    criaPaginacao($Request, $Response);
});

$app->get('/{pagina}/', function(ServerRequestInterface $Request, ResponseInterface $Response){
    criaPaginacao($Request, $Response);
});

$app->run();
