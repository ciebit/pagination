<?php
ini_set('display_errors', on);
require_once('../vendor/autoload.php');

use Ciebit\DomTemplate;
use Ciebit\Pagination\Paginator;
use Ciebit\Pagination\Views\ByDefault as DefaultView;
use Ciebit\Pagination\Factories\ByDefault as Factory;

$Factory = new Factory;
$Factory->setView(DefaultView::class);
$View = $Factory->build(40, 5, 1);
$View->update();
echo $View->getHtml();
